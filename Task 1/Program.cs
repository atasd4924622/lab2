﻿using System;
using System.Diagnostics.CodeAnalysis;

class Program
{
    static uint seed;
    static uint CustomRandom()
    {
        uint a = 1140671485;
        uint c = 12820163;
        uint m = 16777216;
        seed = (a * seed + c) % m;
        return seed;
    }

    static void Main()
    {
        int[] intervalCounts = new int[251];
        uint[] randomNumbers = new uint[20000];
        double[] probabilities = new double[251];
        double sum = 0;
        double ms, dis = 0, vid;

        for (int i = 0; i < 20000; i++)
        {
            randomNumbers[i] = CustomRandom() % 250;
            Console.WriteLine("Число: " + randomNumbers[i]);
        }

        for (uint i = 0; i < 250; i++)
        {
            intervalCounts[i] = 0;
            for (int j = 0; j < 20000; j++)
            {
                if (randomNumbers[j] == i)
                {
                    intervalCounts[i]++;
                }
            }
            probabilities[i] = (double)intervalCounts[i] / 20000;
            sum += i * probabilities[i];
            Console.WriteLine($"Число {i}, частота {intervalCounts[i]}, статична ймовірність {probabilities[i]}");
        }

        ms = sum;
        Console.WriteLine($"Математичне сподівання: {ms}");

        for (int i = 0; i < 250; i++)
        {
            dis += probabilities[i] * Math.Pow(i - ms, 2);
        }
        Console.WriteLine($"Дисперсія випадкових величин: {dis}");

        vid = Math.Sqrt(dis);
        Console.WriteLine($"Середньоквадратичне відхилення випадкових величин: {vid}");
    }
}
